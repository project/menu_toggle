
if(Drupal.jsEnabled) {
	$(document).ready(function() {
		if(Drupal.settings.menu_toggle_default == 0) {
			$('li.expanded').not('li.active-trail').removeClass('expanded').addClass('collapsed');
			$('li.collapsed > ul').hide();
		}
		$('li.expanded > a, li.collapsed > a').click(function(e) {
			e.preventDefault();
			$(this).next('ul').toggle();
			$(this).parent('li').toggleClass('expanded').toggleClass('collapsed');
		});
	});
}